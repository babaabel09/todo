const addBtn = document.getElementById("btn");
const form = document.getElementById("form");
let list = document.querySelector(".unorder-list");
// const strike = document.querySelector(".strike");

let content = JSON.parse(localStorage.getItem("myList")) || [] ;

function sortPrior(e) {
    content.sort(function (a,b) {
        if (a.priority > b.priority) {
        return 1;  
        }else{
          return -1;
        }
    });
}

let showTodo = () =>{
list.innerHTML = content.map((x, id) =>{
   let isCompleted = x.status == "completed" ? "checked" : "";               
    return `
<div class="li ${x.priority} rmv-pror">
<li class="strike ${isCompleted}">${x.value}</li>
<div  class="input">
<button class="check-1"><input id="${id}"  type="checkbox" onchange="checkBtn(this)" ${isCompleted}  class="check" ></button> 
<i class="fa-solid fa-minus " onclick="rmvBtn(${id})"></i>
</div>
</div> 
    `
}).join("")
displayHcont();

}
showTodo();
form.addEventListener('submit', (e) =>{
    e.preventDefault();
 let value = document.getElementById("input-1");
 let list = document.querySelector(".unorder-list");
if (value.value == 0 ) {
        alert("write someting oga!!!")
        return;
}else{
 
let div1 = document.createElement("div");
        div1.classList.add("li");

let li = document.createElement("li");
        li.classList.add("strike");
        li.textContent = value.value;

let div2 = document.createElement("div");
        div2.classList.add("input");

let btn = document.createElement("button");
        btn.classList.add("check-1");

var x = document.createElement("INPUT");
        x.setAttribute("type", "checkbox");
        x.classList.add("check");
        x.setAttribute("onclick", "checkBtn(this)")

let icon = document.createElement("i");
icon.classList.add("fa-solid", "fa-minus")
icon.setAttribute("onclick", "rmvBtn(this)")

list.appendChild(div1)
div1.appendChild(li);
div2.appendChild(btn);
btn.appendChild(x);
div1.appendChild(div2);
div2.appendChild(icon);

    const List = {
    value: value.value,
    status: "pending", 
    priority: e.target.category.value,  
}
content.push(List);
value.value = "";
localStorage.setItem("myList", JSON.stringify(content));
sortPrior();
showTodo();

}
});

function sort(e) {
let li = document.querySelectorAll(".li");
let liArr =Array.from(li)
liArr.forEach(function(lii){
let under = lii.firstElementChild;
switch (e.value) {
    case "All":
        if (e.value === "All") {
            lii.classList.remove("show-li")
            // lii.classList.remove("rmv-pror");
        }
        break;

    case "Complete": 
        if(under.classList.contains("checked")){
            lii.classList.remove("show-li"); 
    }else{
        lii.classList.add("show-li"); 
        }
        break;

    case "Incomplete":
        if(!under.classList.contains("checked")){
            lii.classList.remove("show-li"); 
    }else{
        lii.classList.add("show-li"); 
        }
        break;
}
})
 
formReset();
showClear();

// showReset();
     
 }
 
 function prior(e) {

    let li = document.querySelectorAll(".li");
    let liArr =Array.from(li);
    liArr.forEach(function(lii){
    let under = lii.firstElementChild;
      switch (e.value) {
        case "None":
            if (e.value === "None") {
                // lii.classList.remove("show-li")
                lii.classList.remove("rmv-pror");
            };

            break;

        case "Not-important":
                if (lii.classList.contains("C-not-important")) {
                    lii.classList.remove("rmv-pror");               
                }else{  
                    lii.classList.add("rmv-pror");           
                };
                 
                break;
        case "Important":
                    if (lii.classList.contains("B-important")  ) {
                         lii.classList.remove("rmv-pror");
                    }else{
                        lii.classList.add("rmv-pror");
                    }    
                    break;
                    case "Very-important":
                    form.classList.add("rmv-form");

                    if (lii.classList.contains("A-very-important") ) {
                             lii.classList.remove("rmv-pror");
                        }else{             
                            lii.classList.add("rmv-pror");                
                        }                                 
        default:
            break;
      }
    })
    formReset();
    showClear();
 }
 

 function checkBtn(e) {
    let line = e.parentElement.parentElement.parentElement.firstElementChild;
     if(e.checked){
     line.classList.add("checked");
     content[e.id].status = "completed";
     }else{
     line.classList.remove("checked");
     content[e.id].status = "pending";
     }
     localStorage.setItem("myList", JSON.stringify(content)) 
     }
 
 function rmvBtn(e) {
    content.splice(e, 1)
    localStorage.setItem("myList", JSON.stringify(content));
    showTodo();
          }
 
function reset() {
    let prior = document.getElementById("prior");
    let sort = document.getElementById("sort");
    prior.value = "None";
    sort.value = "All";
    showTodo();
    formReset();
    showClear();

}

function showClear(e) {
let clearAll = document.getElementById("clearAll");
let prior = document.getElementById("prior");
let sort = document.getElementById("sort");
if ( prior.value === "None" &&  sort.value === "All") {
    clearAll.style.display = "flex";
}else{
    clearAll.style.display = "none";

}

}

    
function clearArr() {
    content = [];
    localStorage.setItem("myList", JSON.stringify(content));
    showTodo();
    reset();
}

function formReset(e) {
    let prior = document.getElementById("prior");
    let sort = document.getElementById("sort");
    if ( prior.value === "None" &&  sort.value === "All") {
          form.style.display = "block";
    }else{
        form.style.display = "none";
    } 
}

function displayHcont(e) {

    let prior = document.getElementById("prior");
    let sort = document.getElementById("sort");
    let li = document.querySelectorAll(".li");
    let liArr =Array.from(li);
    liArr.forEach(function(lii){
        if ( prior.value === "None" &&  sort.value === "All") {
            lii.classList.remove("rmv-pror")
        }
    })
    };
    displayHcont();